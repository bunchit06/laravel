<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function daftar()
    {
        return view('halaman.register');
    }
    public function kirim(Request $request)
    {
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];

        return view('halaman.welcome', compact('namadepan','namabelakang'));
    }
}
